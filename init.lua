-- Preload the modules we're going to need
require("hs.chooser")
require("hs.webview")
require("hs.drawing")

 -- Reloads config file on save
 function reloadConfig(files)
    doReload = false
    for _,file in pairs(files) do
        if file:sub(-4) == ".lua" then
            doReload = true
        end
    end
    if doReload then
        hs.reload()
    end
end
myWatcher = hs.pathwatcher.new(os.getenv("HOME") .. "/.hammerspoon/", reloadConfig):start()
hs.notify.show("Config loaded","","")

local presFont = "Standard"
local prezBasePath = "../Desktop/Containment_Sentiment/"
local fileBasePath = "file://".. os.getenv("HOME") .."/Desktop/Containment_Sentiment/"


-- Storage for persistent screen objects
local presentationControl = nil
local presentationScreen = nil
local slideBackground = nil
local slideHeader = nil
local slideBody = nil
local slideFooter = nil
local slideModal = nil

-- Configuration for persistent screen objects
local slideFontColor = hs.drawing.color.x11["black"]
local slideBGColor = hs.drawing.color.x11["black"] --slideBGColor

local slideHeaderSize = nil
local slideHeaderFont = presFont

local slideBodyFont = presFont
local slideBodySize = nil

local slideFooterFont = presFont
local slideFooterSize = nil

local slideTitleFont = presFont

-- Metadata for slide progression
local startSlide = 1
local currentSlide = 0

-- Storage for transient screen objects
local refs = {}

-- List of safe default apps
local defaultapps = {"Notes", "Notes", "Messages" , "Safari"} --"Calculator", "Chess", 


function spairs(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end

    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys 
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end

    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end

function launchdefaultapps()
    for k,v in pairs(defaultapps) do
        hs.application.launchOrFocus(v)
    end
end

function killdefaultapps()
    for k,v in pairs(defaultapps) do
        local app = hs.application.get(v)
        if app then
            app:kill()
        end
    end
end
 

function makewebview(name, place, url, html, level)
    if refs[name] then
        return refs[name]
    else
        print("Creating webview "..name)
        local frame = presentationScreen:fullFrame()
        local webViewRect
        if place == "right" then
            local x = frame["x"] + ((frame["w"] - 100)*0.66) + 10
            local y = slideHeader:frame()["y"] + slideHeader:frame()["h"] + 10
            local w = ((frame["w"] - 100)*0.33) - 10
            local h = slideBody:frame()["h"]
            webViewRect = hs.geometry.rect(x, y, w, h)
        elseif place == "body" then
            webViewRect = hs.geometry.rect(frame["x"] + 50,
                             slideHeader:frame()["y"] + slideHeader:frame()["h"] + 10,
                             (frame["w"] - 100),
                             (frame["h"] / 10) * 8 - (frame["h"] / 12))
        else
            webViewRect = frame
        end
        local webview = hs.webview.new(webViewRect)

        if level then
        --webview:level(hs.drawing.windowLevels[level])
        webview:bringToFront(true)
        else
            webview:level(hs.drawing.windowLevels["normal"]+1)
            print("poop")
        end
        webview:windowStyle("closable")
        webview:shadow(true)
        if url then
            webview:url(url)
        elseif html then
            webview:html(html)
        else
            webview:html("NO CONTENT!")
        end
        refs[name] = webview
        return webview
    end
end

function makeWebviewArray(path, prefix) 
    local wvArr = getFolderImgs(path) --gets files

    for k,v in spairs(files, function(t,a,b) return t[b] < t[a] end) do
        print(k,v)
        local fpath = path..v
        makewebview(name, place, url, html, level)
    end

end

function makeBG(color,level)

    --makes a background color 'screen' to hide desktop
    -- add to refs as BG, returns object

    local BG
    BG = hs.drawing.rectangle(presentationScreen:fullFrame())        
    BG:setFillColor(hs.drawing.color.x11[color])
    BG:setFill(true)

    if level then
        BG:setLevel(hs.drawing.windowLevels[level])
    else    
        BG:bringToFront(true)
    end
    refs["BG"] = BG
    return BG
end    


-- get imgs from folder
function getFolderImgs(path)
-- working code to get files from folder using applescript

    local fpath = "set folderPath to POSIX file (\""..hs.fs.pathToAbsolute(path).."\") as alias\n"
    local asStr = [[
        tell application "System Events" to get POSIX path of every file in the folderPath
        tell application "System Events" to set file_list to name of every file in the folderPath whose visible = true
        return file_list
    ]]

    asStr = fpath..""..asStr
    ok,result = hs.osascript.applescript(asStr)

    --print(ok)
    --print(asStr)
    --print(fpath)

    return result

end

-- Constructs a background Image on a slide
function getFolderFiles(path)

    local files = getFolderImgs(path) -- get files
    
    for k,v in spairs(files, function(t,a,b) return t[b] < t[a] end) do
        print(k,v)
        local fpath = path..v
        hs.open(fpath)
    end

    -- for k,v in pairs(files) do
    --     local fpath = path..v
    --     hs.open(fpath)
    -- end

end


-- Constructs a background Image on a slide
function makebgImage(name, path, padding, color)
    if refs[name] then
        return refs[name]
    else
        print("Creating BG Image "..name)
        local frame = presentationScreen:fullFrame()
        local imgViewRect
        if padding then
            local x = frame["x"] + padding / 2
            local y = frame["y"] + padding / 2
            local w = frame["w"] - padding + 15
            local h = frame["h"] - padding
            imgViewRect = hs.geometry.rect(x, y, w, h)
        else
            imgViewRect = presentationScreen:fullFrame()   
        end
        local slidebgImg = hs.drawing.image(imgViewRect, path)
      
        if color then

            local BG
            BG = hs.drawing.rectangle(frame)        
            BG:setLevel(hs.drawing.windowLevels["normal"])
            BG:setFillColor(hs.drawing.color.x11[color])
            BG:setFill(true)
            BG:show(0.2)
            refs["BG"] = BG
        end

        -- Debugginh
        --local slidebgImg = hs.drawing.rectangle(imgViewRect)
        --slidebgImg:setFillColor(hs.drawing.color.hammerspoon["osx_red"])
        --slidebgImg:setFill(true)
        slidebgImg:setLevel(hs.drawing.windowLevels["normal"])
        refs[name] = slidebgImg
        return slidebgImg
    end
end

-- Definitions of the slides
local slides = {
  
    {
        ["enterFn"] = function()
            local frame = presentationScreen:fullFrame()
            --make a black background so you don't see the desktop underneath
            --hide it after the set of images
            local blackBG = makeBG("black")
            blackBG:show()

            local slideHeader = hs.drawing.text(hs.geometry.rect(frame["x"] + 50,
            frame["y"] + 50,
            frame["w"] - 100,
            frame["h"] / 10),
            "Containment Sentiment")
            slideHeader:setTextColor(hs.drawing.color.x11["white"])
            slideHeader:setTextFont(slideHeaderFont)
            slideHeader:setTextSize(slideHeaderSize)
            slideHeader:orderAbove(blackBG)

            refs["title"] = slideHeader
            slideHeader:show(0.3)

        end,
        ["exitFn"] = function()
            local webview = refs["title"]
            webview:hide(0.2)
            local background = refs["BG"] 
            background:hide(0.2)
        end,
    },

    -- 02
    {
        ["header"] = "Arrangement",
    },

    { --a
        ["enterFn"] = function()
            killdefaultapps()
            getFolderFiles(prezBasePath.."02/02a/")
        end,
    },

    { -- b
        ["enterFn"] = function()
            killdefaultapps()
            getFolderFiles(prezBasePath.."02/02b/")
        end,
    },

    -- 03

    { -- a
        ["header"] = "Complex Set of Relations",
    },

    {
        ["enterFn"] = function()
            local webview = makewebview("3", nil, fileBasePath .. "03/video/interviews.html", nil, "_MaximumWindowLevelKey" )
            webview:show(0.3)
        end,
        ["exitFn"] = function()
            local webview = refs["3"]
            webview:hide(0.2)
        end,
    },

    -- { -- b. Maps
    -- ["enterFn"] = function()
    --     -- local webview = makewebview("maps", "body", "https://www.google.com/maps/d/viewer?mid=1837Dm2bjkHEYNBD-u8yzuAqemgcBgDaX&ll=34.11050660207041%2C-118.33474749843754&z=12", nil)
    --     -- webview:show(0.3)
    --     hs.urlevent.openURL("https://www.google.com/maps/d/viewer?mid=1837Dm2bjkHEYNBD-u8yzuAqemgcBgDaX&ll=34.11050660207041%2C-118.33474749843754&z=12")
    --     local safariW = s.window.frontmostWindow()
    --     safariW:maximize() 
    -- end,
    -- },

    --audio
    -- {
    --     ["enterFn"] = function()

    --     --make a black background so you don't see the desktop underneath
    --     --hide it after the set of images
    --     -- local whiteBG = makeBG("white")
    --     -- whiteBG:show()

    --     local webview = makewebview("03c", nil, fileBasePath .. "03/New_Edits/html/03.html", nil, "_MaximumWindowLevelKey" )
    --     webview:show(0.2)
    --     end,
    --     ["exitFn"] = function()
    --         local webview = refs["03c"]
    --         webview:hide(0.2)
    --     end,
    -- },
    -- {
    --     ["enterFn"] = function()

    --     local webview = makewebview("03a", nil, fileBasePath .. "03/New_Edits/html/01.html", nil, "_MaximumWindowLevelKey" )
    --     webview:show(0.2)
    --     end,
    --     ["exitFn"] = function()
    --         local webview = refs["03a"]
    --         webview:hide(0.2)
    --     end,
    -- },
    -- {
    --     ["enterFn"] = function()

    --     local webview = makewebview("03b", nil, fileBasePath .. "03/New_Edits/html/02.html", nil, "_MaximumWindowLevelKey" )
    --     webview:show(0.2)
    --     end,
    --     ["exitFn"] = function()
    --         local webview = refs["03b"]
    --         webview:hide(0.2)
    --     end,
    -- },
  
    -- {
    --     ["enterFn"] = function()

    --     local webview = makewebview("03d", nil, fileBasePath .. "03/New_Edits/html/04.html", nil, "_MaximumWindowLevelKey" )
    --     webview:show(0.2)
    --     end,
    --     ["exitFn"] = function()
    --         local webview = refs["03d"]
    --         webview:hide(0.2)
    --         -- local BG = refs["BG"]
    --         -- whiteBG:hide(0.2)
    --     end,
    -- },


    -- 04
    {
        ["header"] = "Sequence of Events",
    },
    -- { -- a. Stuttgart Website
    --     ["enterFn"] = function()
    --         hs.urlevent.openURL("https://salonstuttgart.com/exhibitions/fiona-connor-1")
    --         local safariW = s.window.frontmostWindow()
    --         safariW:maximize() 
    --     end,
    -- },

    { -- b. Images 
    ["enterFn"] = function()
        getFolderFiles(prezBasePath.."04/04b/")
    end,
    },
    -- b_Full Images 
 
    {
        ["enterFn"] = function()

            --make a black background so you don't see the desktop underneath
            --hide it after the set of images
            local blackBG = makeBG("black")
            blackBG:show()

            --preload all these foos to prevent flicker
            local webview1 = makewebview("4b_1", nil, fileBasePath .. "04/04b_Full/html/04b_01.html", nil, "_MaximumWindowLevelKey" )
            webview1:alpha(0)

            local webview2 = makewebview("4b_2", nil, fileBasePath .. "04/04b_Full/html/04b_02.html", nil, "_MaximumWindowLevelKey" )
            webview2:alpha(0)

            local webview3 = makewebview("4b_3", nil, fileBasePath .. "04/04b_Full/html/04b_03.html", nil, "_MaximumWindowLevelKey" )
            webview3:alpha(0)

            local webview4 = makewebview("4b_4", nil, fileBasePath .. "04/04b_Full/html/04b_04.html", nil, "_MaximumWindowLevelKey" )
            webview4:alpha(0)

            local webview5 = makewebview("4b_5", nil, fileBasePath .. "04/04b_Full/html/04b_05.html", nil, "_MaximumWindowLevelKey" )
            webview5:alpha(0)

            local webview6 = makewebview("4b_6", nil, fileBasePath .. "04/04b_Full/html/04b_06.html", nil, "_MaximumWindowLevelKey" )
            webview6:alpha(0)

            local webview7 = makewebview("4b_7", nil, fileBasePath .. "04/04b_Full/html/04b_07.html", nil, "_MaximumWindowLevelKey" )
            webview7:alpha(0)


            local webview = refs["4b_1"]
            webview:show(0.2)
        end,
        ["exitFn"] = function()
            local webview = refs["4b_1"]
            webview:hide(0.2)
        end,
    },
    {
        ["enterFn"] = function()
            local webview = refs["4b_2"]
            webview:show(0.2)
        end,
        ["exitFn"] = function()
            local webview = refs["4b_2"]
            webview:hide(0.2)
        end,
    },
    {
        ["enterFn"] = function()
            local webview = refs["4b_3"]
            webview:show(0.3)
        end,
        ["exitFn"] = function()
            local webview = refs["4b_3"]
            webview:hide(0.2)
        end,
    },
    {
        ["enterFn"] = function()
            local webview = refs["4b_4"]
            webview:show(0.3)
        end,
        ["exitFn"] = function()
            local webview = refs["4b_4"]
            webview:hide(0.2)
        end,
    },
    {
        ["enterFn"] = function()
            local webview = refs["4b_5"]
            webview:show(0.3)
        end,
        ["exitFn"] = function()
            local webview = refs["4b_5"]
            webview:hide(0.2)
        end,
    },
    {
        ["enterFn"] = function()
            local webview = refs["4b_6"]
            webview:show(0.3)
        end,
        ["exitFn"] = function()
            local webview = refs["4b_6"]
            webview:hide(0.2)
        end,
    },
    {
        ["enterFn"] = function()
            local webview = refs["4b_7"]
            webview:show(0.3)
        end,
        ["exitFn"] = function()
            local webview = refs["4b_7"]
            webview:hide(0.2)
        end,
    },


    -- d. Post card book
    {
        ["enterFn"] = function()
            local webview = makewebview("4d", nil, fileBasePath .. "04/04d/4d.html", nil, "_MaximumWindowLevelKey" )
            webview:show(0.3)
        end,
        ["exitFn"] = function()
            local webview = refs["4d"]
            webview:hide(0.2)
            local currentBG = refs["BG"]
            currentBG:hide(0.2)
        end,
    },

    { -- goose
    ["enterFn"] = function()
        getFolderFiles(prezBasePath.."/04/04b_other/")
    end,
    },

    -- { -- e. Trash Can
    --     ["enterFn"] = function()
    --         killdefaultapps()
    --         local slideImg = makebgImage("tc", prezBasePath.."/04/04e/04e_1.jpg", 100)
    --         slideImg:show(0.3)
    --     end,
    --     ["exitFn"] = function()
    --         local slideImg = refs["tc"]
    --         slideImg:hide(0.2)
    --     end,
    -- },

    -- 05
    {
        ["header"] = "Archival Impulse",
    },
    
    --a

    {
        ["enterFn"] = function()
            --make a black background so you don't see the desktop underneath
            --hide it after the set of images
            local blackBG = makeBG("black")
            blackBG:show()

            --preload all these foos to prevent flicker
            local webview1 = makewebview("5a_1", nil, fileBasePath .. "05/05a/html/05a_01.html", nil, "_MaximumWindowLevelKey" )
            webview1:alpha(0)

            local webview2 = makewebview("5a_2", nil, fileBasePath .. "05/05a/html/05a_02.html", nil, "_MaximumWindowLevelKey" )
            webview2:alpha(0)

            local webview3 = makewebview("5a_3", nil, fileBasePath .. "05/05a/html/05a_03.html", nil, "_MaximumWindowLevelKey" )
            webview3:alpha(0)

            local webview4 = makewebview("5a_4", nil, fileBasePath .. "05/05a/html/05a_04.html", nil, "_MaximumWindowLevelKey" )
            webview4:alpha(0)

            local webview5 = makewebview("5a_5", nil, fileBasePath .. "05/05a/html/05a_05.html", nil, "_MaximumWindowLevelKey" )
            webview5:alpha(0)

            local webview6 = makewebview("5a_6", nil, fileBasePath .. "05/05a/html/05a_06.html", nil, "_MaximumWindowLevelKey" )
            webview6:alpha(0)

            local webview7 = makewebview("5a_7", nil, fileBasePath .. "05/05a/html/05a_07.html", nil, "_MaximumWindowLevelKey" )
            webview7:alpha(0)

            local webview8 = makewebview("5a_8", nil, fileBasePath .. "05/05a/html/05a_08.html", nil, "_MaximumWindowLevelKey" )
            webview7:alpha(0)

            local webview = refs["5a_1"]
            webview:show(0.2)
        end,
        ["exitFn"] = function()
            local webview = refs["5a_1"]
            webview:hide(0.2)
        end,
    },
    {
        ["enterFn"] = function()
            local webview = refs["5a_2"]
            webview:show(0.2)
        end,
        ["exitFn"] = function()
            local webview = refs["5a_2"]
            webview:hide(0.2)
        end,
    },
    {
        ["enterFn"] = function()
            local webview = refs["5a_3"]
            webview:show(0.3)
        end,
        ["exitFn"] = function()
            local webview = refs["5a_3"]
            webview:hide(0.2)
        end,
    },
    -- {
    --     ["enterFn"] = function()
    --         local webview = refs["5a_4"]
    --         webview:show(0.3)
    --     end,
    --     ["exitFn"] = function()
    --         local webview = refs["5a_4"]
    --         webview:hide(0.2)
    --     end,
    -- },
    -- {
    --     ["enterFn"] = function()
    --         local webview = refs["5a_5"]
    --         webview:show(0.3)
    --     end,
    --     ["exitFn"] = function()
    --         local webview = refs["5a_5"]
    --         webview:hide(0.2)
    --     end,
    -- },
    -- {
    --     ["enterFn"] = function()
    --         local webview = refs["5a_6"]
    --         webview:show(0.3)
    --     end,
    --     ["exitFn"] = function()
    --         local webview = refs["5a_6"]
    --         webview:hide(0.2)
    --     end,
    -- },
    -- {
    --     ["enterFn"] = function()
    --         local webview = refs["5a_7"]
    --         webview:show(0.3)
    --     end,
    --     ["exitFn"] = function()
    --         local webview = refs["5a_7"]
    --         webview:hide(0.2)
    --     end,
    -- },
    -- {
    --     ["enterFn"] = function()
    --         local webview = refs["5a_8"]
    --         webview:show(0.3)
    --     end,
    --     ["exitFn"] = function()
    --         local webview = refs["5a_8"]
    --         webview:hide(0.2)
    --         local currentBG = refs["BG"]
    --         currentBG:hide(0.2)
    --     end,
    -- },

    { -- b. Fiona Website
    ["enterFn"] = function()
        hs.urlevent.openURL("http://fconnor.studio")
        local safariW = s.window.frontmostWindow()
        safariW:maximize() 
    end,

    },

    -- 06
    {
        ["header"] = "Classroom Furniture",
    },
    -- { -- a.
    -- ["enterFn"] = function()
    --     killdefaultapps()
    --     getFolderFiles(prezBasePath.."06/06a/")
    -- end,
    -- },

    { -- b.
    ["enterFn"] = function()
        killdefaultapps()
        getFolderFiles(prezBasePath.."06/06b/")
    end,
    },

    { -- c. Email
    ["enterFn"] = function()
        hs.urlevent.openURL("https://drive.google.com/drive/folders/1NelY-oxGccEPk9obpZ2Emk0V2vVow_fY?usp=sharing")
        local safariW = s.window.frontmostWindow()
        safariW:maximize() 
    end,
    },

    { -- d. Varese Group
    ["enterFn"] = function()
        hs.urlevent.openURL("http://www.varese.group")
        local safariW = s.window.frontmostWindow()
        safariW:maximize() 
    end,
    }, 
    
    -- { -- d. A Letter
    -- ["enterFn"] = function()
    --     hs.urlevent.openURL("http://alettertotheunwrittenfuture.org")
    --     local safariW = s.window.frontmostWindow()
    --     safariW:maximize() 
    -- end,
    -- },   

    -- 07
    {
        ["header"] = "Melting",
    },
    { -- a.
    ["enterFn"] = function()
        killdefaultapps()
        getFolderFiles(prezBasePath.."07/")
        hs.application.launchOrFocus("Photo Booth")
        hs.application.launchOrFocus("Notes")
        hs.application.launchOrFocus("Messages")
    end,
    ["exitFn"] = function()
        killdefaultapps()
    end,
    },
    -- 08
    {
        ["enterFn"] = function()
            local frame = presentationScreen:fullFrame()
            --make a black background so you don't see the desktop underneath
            --hide it after the set of images
            local blackBG = makeBG("black")
            blackBG:show()

            local slideHeader = hs.drawing.text(hs.geometry.rect(frame["x"] + 50,
            frame["y"] + 50,
            frame["w"] - 100,
            frame["h"] / 10),
            "Colophon")
            slideHeader:setTextColor(hs.drawing.color.x11["white"])
            slideHeader:setTextFont(slideHeaderFont)
            slideHeader:setTextSize(slideHeaderSize)
            slideHeader:orderAbove(blackBG)

            local slideFooter = hs.drawing.text(hs.geometry.rect(frame["x"] + 50, --horizontally center
            frame["y"] + frame["h"] - 300,
            frame["w"]/ 2.5,
            frame["h"] / 4),
            "Design: Neil Doshi\nInterviews: Tina Odermatt\nPhotos: Kyle Knodell, Oliver Ottenschläger, Fredrik Nilsen, Jessica Mauer, Alex North, Marten Elder\nAdapted for Zoom\n2020"
            )
            slideFooter:setTextColor(hs.drawing.color.x11["white"])
            slideFooter:setTextFont(slideFooterFont)
            slideFooter:setTextSize(slideFooterSize)
            slideFooter:orderAbove(slideBackground)

            refs["title"] = slideHeader
            slideHeader:show(0.3)

            refs["footer"] = slideFooter
            slideFooter:show(0.3)

        end,
        ["exitFn"] = function()
            local webview = refs["title"]
            webview:hide(0.2)
            local background = refs["BG"] 
            background:hide(0.2)
        end,
    },
}

-- Draw a slide on the screen, creating persistent screen objects if necessary
function renderSlide(slideNum)
    print("renderSlide")
    if not slideNum then
        slideNum = currentSlide
    end
    print("  slide number: "..slideNum)

    local slideData = slides[slideNum]
    local frame = presentationScreen:fullFrame()

    -- if not slideBackground then
    --     slideBackground = hs.drawing.rectangle(frame)        
    --     slideBackground:setLevel(hs.drawing.windowLevels["normal"])
    --     slideBackground:setFillColor(slideBGColor)
    --     slideBackground:setFill(true)
    --     slideBackground:show(0.2)
    -- end

    

    if not slideHeader then
        slideHeader = hs.drawing.text(hs.geometry.rect(frame["x"] + 50,
                                                       frame["y"] + 50,
                                                       frame["w"] - 100,
                                                       frame["h"] / 10),
                                                       "")
        slideHeader:setTextColor(slideFontColor)
        slideHeader:setTextFont(slideHeaderFont)
        slideHeader:setTextSize(slideHeaderSize)
        slideHeader:orderAbove(slideBackground)
    end

    slideHeader:setText(slideData["header"] or "")
    slideHeader:show(0.5)

        if not slideBody then
            slideBody = hs.drawing.text(hs.geometry.rect(frame["x"] + 50,
                                                        slideHeader:frame()["y"] + slideHeader:frame()["h"] + 10,
                                                        (frame["w"] - 100)*0.66,
                                                        (frame["h"] / 10) * 8),
                                                        "")
            slideBody:setTextColor(slideFontColor)
            slideBody:setTextFont(slideBodyFont)
            slideBody:setTextSize(slideBodySize)
            slideBody:orderAbove(slideBackground)
        end

        slideBody:setText(slideData["body"] or "")
        slideBody:show(0.5)

    if not slideFooter then
        slideFooter = hs.drawing.text(hs.geometry.rect(frame["w"] / 2, --horizontally center
                                                       frame["y"] + frame["h"] - 50 - slideFooterSize,
                                                       frame["w"] - 100,
                                                       frame["h"] / 25),
                                                       "" -- put something here for persitent text 
                                                    )
        slideFooter:setTextColor(slideFontColor)
        slideFooter:setTextFont(slideFooterFont)
        slideFooter:setTextSize(slideFooterSize)
        slideFooter:orderAbove(slideBackground)
    end

    slideFooter:setText(slideData["footer"] or "")
    slideFooter:show(0.5)

    --title card
    slideTitle = hs.drawing.text(hs.geometry.rect(0, --x
                                                    frame["h"] / 2, --y
                                                    frame["w"], --w
                                                    frame["h"] / 25 --h
                                                    )
                                                )
    slideTitle:setTextFont(slideTitleFont)
    slideTitle:setTextSize(slideFooterSize)
    slideTitle:orderAbove(slideBackground)
    slideTitle:setTextStyle({alignment="center"})
    slideTitle:setText(slideData["title"] or "")
    slideTitle:show(0.5)



end

-- Move one slide forward
function nextSlide()
    if currentSlide < #slides then
        if slides[currentSlide] and slides[currentSlide]["exitFn"] then
            print("running exitFn for slide")
            slides[currentSlide]["exitFn"]()
        end

        currentSlide = currentSlide + 1
        renderSlide()

        if slides[currentSlide] and slides[currentSlide]["enterFn"] then
            print("running enterFn for slide")
            slides[currentSlide]["enterFn"]()
        end
    end
end

-- Move one slide back
function previousSlide()
    if currentSlide > 1 then
        if slides[currentSlide] and slides[currentSlide]["exitFn"] then
            print("running exitFn for slide")
            slides[currentSlide]["exitFn"]()
        end

        currentSlide = currentSlide - 1
        renderSlide()

        if slides[currentSlide] and slides[currentSlide]["enterFn"] then
            print("running enterFn for slide")
            slides[currentSlide]["enterFn"]()
        end
    end
end

-- Exit the presentation
function endPresentation()
    hs.caffeinate.set("displayIdle", false, true)
    if slides[currentSlide] and slides[currentSlide]["exitFn"] then
        print("running exitFn for slide")
        slides[currentSlide]["exitFn"]()
    end
    slideHeader:hide(0.5)
    slideBody:hide(0.5)
    slideFooter:hide(0.5)
    slideBackground:hide(1)
    BG:hide(1)

    hs.timer.doAfter(1, function()
        slideHeader:delete()
        slideBody:delete()
        slideFooter:delete()
        slideBackground:delete()
        slideModal:exit()
    end)
end

-- Prepare the modal hotkeys for the presentation
function setupModal()
    print("setupModal")
    slideModal = hs.hotkey.modal.new({}, nil, nil)

    slideModal:bind({}, "left", previousSlide)
    slideModal:bind({}, "right", nextSlide)
    slideModal:bind({}, "escape", endPresentation)

    slideModal:enter()
end

-- Callback for when we've chosen a screen to present on
function didChooseScreen(choice)
    if not choice then
        print("Chooser cancelled")
        return
    end
    print("didChooseScreen: "..choice["text"])
    presentationScreen = hs.screen.find(choice["uuid"])
    if not presentationScreen then
        hs.notify.show("Unable to find that screen, using primary screen")
        presentationScreen = hs.screen.primaryScreen()
    else
        print("Found screen")
    end

    setupModal()

    local frame = presentationScreen:fullFrame()
    slideHeaderSize = frame["h"] / 15
    slideBodySize   = frame["h"] / 22
    slideFooterSize = frame["h"] / 30

    nextSlide()
end

-- Prepare a table of screens for hs.chooser
function screensToChoices()
    print("screensToChoices")
    local choices = hs.fnutils.map(hs.screen.allScreens(), function(screen)
        local name = screen:name()
        local id = screen:id()
        local image = screen:snapshot()
        local mode = screen:currentMode()["desc"]

        return {
            ["text"] = name,
            ["subText"] = mode,
            ["uuid"] = id,
            ["image"] = image,
        }
    end)

    return choices
end

-- Initiate the hs.chosoer for choosing a screen to present on
function chooseScreen()
    print("chooseScreen")
    local chooser = hs.chooser.new(didChooseScreen)
    chooser:choices(screensToChoices)
    chooser:show()
end

-- Prepare the presentation
function setupPresentation()
    print("setupPresentation")
    hs.caffeinate.set("displayIdle", true, true)
    chooseScreen()
end

-- Create a menubar object to initiate the presentation
presentationControl = hs.menubar.new()
--presentationControl:setIcon(hs.image.imageFromName(hs.image.systemImageNames["EnterFullScreenTemplate"]))
presentationControl:setIcon(hs.image.imageFromName("NSComputer"))
presentationControl:setMenu({{ title = "Start Presentation", fn = setupPresentation }})

