# Containment Sentiment

## v2. Calarts (Zoom) 2020-04-17

### Instructions

1. If you have a `Containment_Sentiment` folder on your desktop delete it.
2. Move folder `Containment_Sentiment` to Desktop
3. Move file `init.lua` to ~/.hammerspoon. Overwrite the file if it exists.
   i. Navigate to your home folder (the icon with a house in the Finder)
   ii. Enable viewing of hidden files `shift + command + .`
   iii. You should see a folder called .hammerspoon
   iv. Move `init.lua` and overwrite any version that's already in there
4. Done! Test out if everything worked by starting Hammerspoon.

#### Previously

Otis 2020-04-02
